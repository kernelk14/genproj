#!/usr/bin/env python3

# print("Hello World!")

# Configurable script generator for projects.

import os

filename = open("proj", "r")

script = filename.read().split()

out = open('build', 'w')

# namedef = []

def openfile(filename):
    namedef = []
    filedef = []
    for p, op in enumerate(script):
        if script[p] == 'file':
            filedef.append(script[p+1].replace('"', ''))
        if script[p] == 'title':
            filedef = filedef
            namedef = namedef
            namedef.append(script[p+1].replace('"', ''))
            a = namedef.pop()
            out.write(f"cp {filedef} {a}\n")
        if script[p] == 'project':
            namedef = namedef
            a = a
            if script[p+1] == 'run':
                out.write(f"./{a}\n")
    os.system("chmod +x build")
    os.system("./build")
openfile(filename)
